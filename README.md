Billoir directory Virtualization and Container

Q1 :

```mermaid
sequenceDiagram
    docker->>dockerd: Contact
    dockerd->>cache locale: Looking if image is already pull locally
    cache locale->>dockerd: Send
    dockerd->>docker: Send
    dockerd->>hub docker: Request image
    hub docker->>dockerd: Pull image
    dockerd->>cache locale: Store cache
    dockerd->>docker: Output result
```


Q3 :

A single manifest is information about an image, such as layers, size, and digest. The docker manifest command also gives users additional information such as the os and architecture an image was built for.
```
manifest inspect --help

Usage:  docker manifest inspect [OPTIONS] [MANIFEST_LIST] MANIFEST
```

Q4 :

```
REPOSITORY                                    TAG             IMAGE ID       CREATED

busybox                                       1               b97242f89c8a   2 weeks ago     1.23MB
busybox                                       1-uclibc        b97242f89c8a   2 weeks ago     1.23MB
busybox                                       latest          b97242f89c8a   2 weeks ago     1.23MB
busybox                                       uclibc          b97242f89c8a   2 weeks ago     1.23MB
busybox                                       1.31            1c35c4412082   8 months ago    1.22MB
busybox                                       1.30            64f5d945efcc   21 months ago   1.2MB
```

On contaste que les ID des images les plus recentes sont identiques, ils sont donc issus du meme `dockerfile`

Q5 :

Image : Dockerfile
Container : Software qui execute l'image

Q6 :

```
Usage:  docker run [OPTIONS] IMAGE [COMMAND] [ARG...]

Run a command in a new container

Options:
 -i, --interactive                    Keep STDIN open even if not attached
 -t, --tty                            Allocate a pseudo-TTY
```

